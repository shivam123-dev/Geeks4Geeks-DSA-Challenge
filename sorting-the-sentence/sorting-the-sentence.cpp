class Solution {
public:
    string sortSentence(string s) {
        vector<string>st(10, "");
        string s1 = "";
        for(int i=0;i<s.size();i++){
            if(!isdigit(s[i])){
                s1 += s[i];
            }
            else {
                st[s[i]-'0'] = s1;
                s1 = "";
                i++;
            }
        }
        s1 = "";
        for(int i=1;i<st.size();i++){
            if(st[i].size()){
                s1 += st[i] + ' ';
            }
        }
        s1.pop_back();
        return s1;
    }
};