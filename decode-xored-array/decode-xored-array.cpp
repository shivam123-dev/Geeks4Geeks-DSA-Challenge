class Solution {
public:
    vector<int> decode(vector<int>& encoded, int first) {
        vector<int> arr;
        arr.push_back(first);
        for(int i=0;i<encoded.size();i++){
            int ele = first ^ encoded[i];
            first = ele;
            arr.push_back(ele);
        }
        return arr;
    }
};