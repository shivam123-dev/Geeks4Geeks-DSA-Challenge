/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int sum;
    void dfs(TreeNode* root, int low, int high){
        if(!root)
            return;
        if(root->val <= high and root->val >= low)
            sum += root->val;
        // if(root->val < high)
        if(root->right)
            dfs(root->right, low, high);
        // if(root->val > low)
        if(root->left)
            dfs(root->left, low, high);
        return;
    }
    int rangeSumBST(TreeNode* root, int low, int high) {
        sum = 0;
        dfs(root, low, high);
        return sum;
    }
};