class Solution {
public:
    vector<int> smallerNumbersThanCurrent(vector<int>& nums) {
        vector<int> a;
        int i=0;
        for(;i<nums.size();i++){
            int count = 0;
            for(int j=0;j<nums.size();j++){
                if(nums[i]>nums[j])
                    count++;
            }
            a.push_back(count);
        }
        return a;
    }
};