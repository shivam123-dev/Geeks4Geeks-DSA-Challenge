class Solution {
public:
    int numberOfSteps(int num) {
        int count=0;
        while(num!=0){        
            if(num%2 == 0){
                num /= 2;
            }
            else{
                num -= 1;
                count++;
                if(num!=0)
                    num /= 2;
            }
            if(num!=0)
                count++;
        }
        return count;
    }
};