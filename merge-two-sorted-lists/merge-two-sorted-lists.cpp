/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    // Method 1 -> Using recursion
    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
        if(!list1)
            return list2;
        if(!list2)
            return list1;
        if(list1->val < list2->val){
            list1->next = mergeTwoLists(list1->next, list2);
            return list1;
        }
        else{
            list2->next = mergeTwoLists(list1, list2->next);
            return list2;
        }
    }
    // Method 2 -> without using recursion
    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
     
        if(!list1)
            return list2;
        if(!list2)
            return list1;
        ListNode* head = NULL;
        if(list1->val < list2->val){
            head = list1;
            list1 = list1->next;
        }
        else{
            head = list2;
            list2 = list2->next;
        }            
        ListNode* p = head;
        while(list1 and list2){
            if(list1->val < list2->val){
                p->next = list1;
                list1 = list1->next;
            }
            else{
                p->next = list2;
                list2 = list2->next;
            }
            p = p->next;
        }
        if(list1 and !list2){
            p->next = list1;
        }
        else{
            p->next = list2;
        }
        return head;        
    }
};
