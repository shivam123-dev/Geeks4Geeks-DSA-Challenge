class Solution {
public:
    void rotate(vector<int>& nums, int k) {
        vector<int> a;
        int n = nums.size();
        int i=0;
        k = k%n;
        for(int j=n-k;j<n;j++)
            a.push_back(nums[j]);
        for(int j=0;j<n-k;j++)
            a.push_back(nums[j]);        
        nums = a;
    }
};